console.log("S21 Activity");
console.log(" ");

let wrestlers = ['Dwayne Johnson', 'Steve Austin', 'Kurt Angle', 'Dave Baustista'];
console.log("Original Array:");
console.log(wrestlers);

function addWrestler(newWrestler){
	wrestlers[wrestlers.length++] = newWrestler;
}

addWrestler("John Cena");
console.log(wrestlers);




function wrestlerIndex(wIndex){
	return wrestlers[wIndex];
}


let itemFound = wrestlerIndex(2);
console.log(itemFound);



function deleteItem(lastItemDelete){
	return wrestlers[lastItemDelete]
}


let lastItem = deleteItem(wrestlers.length - 1)
console.log(lastItem)
wrestlers.length--;
console.log(wrestlers);


function updateWrestlers(updateIndex){
	wrestlers[updateIndex] = "Triple H";

}


updateWrestlers(3);
console.log(wrestlers)


function emptyArray(){
	wrestlers = [];
}

emptyArray();
console.log(wrestlers);



function isArrayEmpty(){
	if(wrestlers.length === 0){
		return true;
	} else {
		return false;
	}
}

let isUserEmpty = isArrayEmpty();
console.log(isUserEmpty);
